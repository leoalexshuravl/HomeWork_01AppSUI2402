//
//  MainScreen.swift
//  HomeWork_01AppSUI2402
//
//  Created by Журавлев Лев on 13.03.2024.
//

import SwiftUI

struct MainScreen: View {
    
    @EnvironmentObject var tabViewModel: TabViewModel
    @EnvironmentObject var emojisViewModel: EmojisViewModel
    
    var body: some View {
        Button("on secondScreen") {
            tabViewModel.selectedTab = 1
            emojisViewModel.emojis.randomElement()?.isActive = true
        }
    }
}

#Preview {
    MainScreen()
}
