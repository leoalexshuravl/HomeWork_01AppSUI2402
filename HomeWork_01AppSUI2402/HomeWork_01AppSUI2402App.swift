//
//  HomeWork_01AppSUI2402App.swift
//  HomeWork_01AppSUI2402
//
//  Created by Журавлев Лев on 13.03.2024.
//

import SwiftUI

@main
struct HomeWork_01AppSUI2402App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
