//
//  DetailScreen.swift
//  HomeWork_01AppSUI2402
//
//  Created by Журавлев Лев on 13.03.2024.
//

import SwiftUI

struct DetailScreen: View {
    
    let emoji: String
    
    var body: some View {
        DetailScreenKit(emoji: emoji)
    }
}


struct DetailScreenKit: UIViewRepresentable {
    typealias UIViewType = UIView
    let emoji: String
    
    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        view.backgroundColor = .systemBlue
        view.layer.cornerRadius = 8
        
        let label = UILabel()
        label.text = emoji
        label.textAlignment = .center
        
        view.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) { }
    
    
}


#Preview {
    MainScreen()
}
