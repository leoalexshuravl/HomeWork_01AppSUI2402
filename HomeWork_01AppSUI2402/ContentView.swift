//
//  ContentView.swift
//  HomeWork_01AppSUI2402
//
//  Created by Журавлев Лев on 13.03.2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabViewScreen()
    }
}

#Preview {
    ContentView()
}
