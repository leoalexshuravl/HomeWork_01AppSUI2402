//
//  ListScreen.swift
//  HomeWork_01AppSUI2402
//
//  Created by Журавлев Лев on 13.03.2024.
//

import SwiftUI

class EmojisViewModel: ObservableObject {
        
    @Published var emojis: [EmojiModel] = [
        EmojiModel(emoji: "🐶"),
        EmojiModel(emoji: "🐱"),
        EmojiModel(emoji: "🐭"),
        EmojiModel(emoji: "🐹"),
        EmojiModel(emoji: "🦊")
    ]
    
    class EmojiModel: Identifiable {
        let emoji: String
        var id = UUID()
        @Published var isActive: Bool = false
        
        init(emoji: String, isActive: Bool = false) {
            self.emoji = emoji
            self.isActive = isActive
        }
    }
}

struct ListScreen: View {
    
    @EnvironmentObject var emojisViewModel: EmojisViewModel
    
    var body: some View {
        NavigationView {
            List(emojisViewModel.emojis) { emojiModel in
                NavigationLink(emojiModel.emoji, isActive: Binding(
                    get: { emojiModel.isActive },
                    set: { newValue in emojiModel.isActive = newValue })
                ) {
                    DetailScreen(emoji: emojiModel.emoji)
                }.onTapGesture {
                    emojiModel.isActive = true
                }
            }
        }
    }
}

#Preview {
    ListScreen()
}
