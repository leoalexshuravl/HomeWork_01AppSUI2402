//
//  TabViewScreen.swift
//  HomeWork_01AppSUI2402
//
//  Created by Журавлев Лев on 13.03.2024.
//

import SwiftUI

class TabViewModel: ObservableObject {
    @Published var selectedTab: Int = 0
}

struct TabViewScreen: View {
    
    @StateObject private var tabViewModel = TabViewModel()
    @ObservedObject var emojisViewModel = EmojisViewModel()
    
    var body: some View {
        TabView(selection: $tabViewModel.selectedTab) {
            MainScreen()
                .tag(0)
                .tabItem {
                    VStack {
                        Image(systemName: "menucard")
                        Text("mainScreen")
                    }
                }
            ListScreen()
                .tag(1)
                .tabItem {
                    VStack {
                        Image(systemName: "list.dash")
                        Text("listScreen")
                    }
                }
            ThirdScreen()
                .tag(2)
                .tabItem {
                    VStack {
                        Image(systemName: "rectangle.inset.filled.and.person.filled")
                        Text("thirdScreen")
                    }
                }
        }
        .environmentObject(tabViewModel)
        .environmentObject(emojisViewModel)
        
    }
}

#Preview {
    TabViewScreen()
}
