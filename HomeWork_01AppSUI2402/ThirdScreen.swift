//
//  ThirdScreen.swift
//  HomeWork_01AppSUI2402
//
//  Created by Журавлев Лев on 13.03.2024.
//

import SwiftUI

struct ThirdScreen: View {
    
    @State private var isModalPresented = false
    
    var body: some View {
        Button("Show Modal") {
            self.isModalPresented.toggle()
        }
        .sheet(isPresented: $isModalPresented) {
            ModalView()
        }
    }
}

struct ModalView: View {
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack {
            Text("This is a modal view")
                .font(.headline)
                .padding()
            Button("Close") {
                self.presentationMode.wrappedValue.dismiss()
            }
            .padding()
        }
    }
}

#Preview {
    ThirdScreen()
}
